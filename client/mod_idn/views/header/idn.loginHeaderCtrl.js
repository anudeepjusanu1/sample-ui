'use strict';

angular.module('mod.idn')
    .controller('loginHeaderCtrl', ['$scope', '$window', 'CLIENT_CONFIG', function ($scope, $window, CLIENT_CONFIG) {

        $scope.isLogin = false;

        $scope.website = function(){
            var url = CLIENT_CONFIG.CLIENT_DOMAIN + '/site'; 
            $window.open(url, "_blank");
        };

        var init = function () {};

        init();
    }])